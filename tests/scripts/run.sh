#!/bin/bash

FAIL_COUNT=0

check_pkg() {
    local pkg=$1
    if rpm -q $pkg
    then
        echo "PASS"
    else
        echo "FAIL"
        FAIL_COUNT=$((${FAIL_COUNT} + 1))
    fi
}

check_binary(){
    COMMAND="$1 $2"
    echo $COMMAND
    ${COMMAND}
    RETVAL=$?
    if [ ${RETVAL} != 0 ] ; then
        echo "FAIL"
        FAIL_COUNT=$((${FAIL_COUNT} + 1))
    else
        echo "PASS"
    fi
}
check_pkg "malaga"
check_pkg "libmalaga"
for i in malaga mallex malmake malrul malshow malsym
do
    check_binary ${i} -v
    check_binary ${i} -h
done
echo "FAIL_COUNT=${FAIL_COUNT}"
exit ${FAIL_COUNT}
